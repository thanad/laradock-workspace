#
#--------------------------------------------------------------------------
# Image Setup
#--------------------------------------------------------------------------
#

FROM phusion/baseimage:0.9.18

MAINTAINER Thanad Pansing <thanad@gmail.com>

RUN DEBIAN_FRONTEND=noninteractive
RUN locale-gen en_US.UTF-8

ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LC_CTYPE=UTF-8
ENV LANG=en_US.UTF-8
ENV TERM xterm

# Install "software-properties-common" (for the "add-apt-repository")
RUN apt-get update && apt-get install -y \
    software-properties-common
    
RUN add-apt-repository ppa:ondrej/php

# Install PHP-CLI, some PHP extentions and some useful Tools with APT
RUN apt-get update && apt-get install -y --force-yes \
        php5.6-cli \
        php5.6-dev \
        php5.6-mysql \
        php5.6-pgsql \
        php5.6-sqlite \
        php5.6-apcu \
        php5.6-json \
        php5.6-curl \
        php5.6-gd \
        php5.6-gmp \
        php5.6-imap \
        php5.6-mcrypt \
        php5.6-memcached \
        php5.6-mbstring \
        php5.6-xml \
        php5.6-zip \
        php-pear \
        libcurl4-openssl-dev \
        libedit-dev \
        libssl-dev \
        libxml2-dev \
        xz-utils \
        sqlite3 \
        libsqlite3-dev \
        git \
        curl \
        vim \
        nano \
        python-pip
        
RUN ln -sfn /usr/bin/php5.6 /etc/alternatives/php

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - && \
    apt-get install -y nodejs

# Install Composer
RUN curl -s http://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

# Install Node.js
RUN npm install -g gulp bower

# pip install awscli
RUN pip install awscli

ADD ./aws/config /root/.aws/config
ADD ./aws/credentials /root/.aws/credentials

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /var/www/laravel